var READTHEDOCS_DATA = {
    "project": "laravel-docs-pt-br", 
    "theme": "readthedocs", 
    "version": "latest", 
    "source_suffix": ".md", 
    "api_host": "https://readthedocs.org", 
    "language": "en", 
    "commit": "76bd820a99b202b69ed15e980f74add0f0c2d60a", 
    "docroot": "/home/docs/checkouts/readthedocs.org/user_builds/laravel-docs-pt-br/checkouts/latest", 
    "builder": "mkdocs", 
    "page": null
}

// Old variables
var doc_version = "latest";
var doc_slug = "laravel-docs-pt-br";
var page_name = "None";
var html_theme = "readthedocs";

READTHEDOCS_DATA["page"] = mkdocs_page_input_path.substr(
    0, mkdocs_page_input_path.lastIndexOf(READTHEDOCS_DATA.source_suffix));
